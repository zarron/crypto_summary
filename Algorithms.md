# Algorithms

## Interactive Proofs

|Name|Algorithm|
|---|---|
|Graph Isomorphism|![graph_isomorphism](images/graph_isomorphism.png)|
|Graph Non Isomorphism|![graph_non_isomorphism](images/graph_non_isomorphism.png)|
|Fiat Shamir|![fiat_shamir](images/fiat_shamir.png)|
|Gillou Quisquater Isomorphism|![gillou_quisquater](images/gillou_quisquater.png)|
|Schnorr|![schnorr](images/schnorr.png)|
|Hamiltonian Cycles|![hamiltonian_cycles](images/hamiltonian_cycles.png)|

## Commitment Schemes
|Name|Algorithm|
|---|---|
|Commitment Schemes|![commitment_schemes](images/commitment_schemes.png)|

## Shamir Sharing
|Name|Algorithm|
|---|---|
|Passive Protocol|![passive_protocol](images/passive_protocol.png)|

## Consensus
|Name|Algorithm|
|---|---|
|Weak Consensus|![weak_consensus](images/weak_consensus.png)|
|Graded Consensus|![graded_consensus](images/graded_consensus.png)|

