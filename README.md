# Cryptographic Protocols Key Concepts 
## Lecture 1

### Proof System
It contains of the following things:
- Statement: A string, that has to be proofed
- Semantic: $`\tau(statement) \rightarrow \{true, false\}`$
- Proof: A string containing a necessary input for the verification function
- Verification Function $`\varphi (statement, proof) \rightarrow \{accept, reject\}`$
  

### Language

E set $`L`$ of strings $`s`$, where $`\tau(s)=true`$.       
### Interactive Proof Systems
If a protocol is used to proof something, i.e. not a single message is sent, but multiple messages are exchanged between proofer and verifier

### Proof Nomenclature
- Soundness: Only true statements have a proof
- Completeness: Every true statement has a proof
- Efficient verifiability: The verification function is efficient to calculate
## Lecture 2

### Hardness
#### P
All problems that can be solved and therefore checked in polynomial time.

#### NP
All problems that can be solved in arbitrary time, but checked in polynomial time. Contains P.

#### NP Complete
A subset of NP problems, where every other problem in NP can quickly be reduced to. So, finding a quick solution to this problem would mean finding a solution to every other problem in NP.

#### NP-Hard
All problems that are as least as hard as the hardest problems in NP (i.e. the NP complete problems)


### Interactive Proof Language
1. Can be verified in polynomial time
2. The probability to accept a correct proof must be higher or equal than $\frac{3}{4}$, but is usually $1$
3. The probability that the proof was not correct must be smaller or equal than $\frac{1}{2}$ 

## Lecture 3

### Simulator
An algorithm that can construct a valid transcript, only knowing the publicly known values.

### Zero Knowledge
It means that a Verifier doesn't have any more information after the protocol then before.
#### Zero Knowledge
For each Verifier V there can be found a Simulator S 

#### Black-Bock Zero Knowledge
There exists a Simulator S that can simulate a protocol for every Verifier V.


#### Honest Verifier Zero Knowledge
There exists a Simulator that can simulate the protocol for a honest Verifier.

#### Perfect Zero Knowledge
If a protocol consists of one or more c-simulatable rounds and the space of c is polynomially bounded, it is perfect zero knowledge.

## Lecture 4
### Proof of Knowledge
If a protocol consists of s 2-extractable, and $`\frac{1}{\mid C\mid^s}`$ is negligible. 

### Knowledge Extractor
A program that can interact with a proofer and can rewind it. 

### 2-extractable
For two tuples (t,c,r) with same t but different c one can compute x.

### General Form
It is possible to proof for a general form of protocols that they are 2-extractable. For this they have to use a one way homomorphism.

## Lecture 5

### Hamiltonian Cycles
A cycle in a graph that visits every node exactly once.

### Commitment Schemes
Protocols, that allow party A to commit to a specific value. The commitment does not leak any information about the committed value, until the committing party decides to reveal it. Once revealed, it is clear that the revealed value was inside the commitment.

#### Perfect Hiding
In perfect hiding commitment schemes, there is o way to discover the committed value, even with unlimited computing power. The binding however is only computationally secure.

#### Perfect Binding
These commitment schemes only hide the value with computational security, but it is completely sure, that the revealed value matches the committed one.

## Lecture 6
Gugus; gugus gsi, gugus gepliben.

## Lecture 7

### Multi Party Computation
Multiple parties want to compute a function together. They all send their input to a trusted party, which computes the output and sends it to all participants. This should be achieved without a trusted party but with a special MPC protocol. Therefore the protocol should enable the exact features as the specification.

### Oblivious Transfer
There are multiple types of possible oblivious transfers.
- Rabin-OT: Receiver randomly receives either the message or nothing ($`s, \bot`$)
- 1-2-OT: Receiver can choose to either receive $`s_0`$ or $`s_1`$
- 1-k-OT: Receiver can choose one of the $`s_i`$ inputs, where $`i \in \{1 \dots k\}`$

It can for example be achieved with RSA encryption.

### Sharing
To share a value among $`k\leq \frac{n}{2}`$ parties, a polygon $`f(x)`$ of degree $`\leq t=k-1`$, such that $`f(0)=s`$, where $`s`$ is the secret, is constructed. Samples $`f(\alpha_i)`$, where $`i\in\{1 \dots n\}`$.
With at least $`k`$ samples the original polynomial can be reconstructed. However, $`f(0)`$ can also be directly computed via Lagrange Interpolation

### Linear Operations
Linear operations can directly be applied to the sharings of the secret, since the sharing scheme itself is linear.

### Multiplication
Multiplication of two sharings is harder. If one would just multiply the two sharings, it would just double the degree of the polynomial and would soon no longer be reconstructible.

But since $`k\leq \frac{n}{2}`$ it is still possible to reconstruct $`c=a*b`$ after one multiplication. But since $`c`$ might be only an intermediate result, it should not be leaked to everybody, and therefore cannot be completely reconstructed. But since Lagrange Interpolation at point 0 is just a linear function of the shared $`d_i = a_i*b_i`$, the $`d_i`$s can be shared again among the parties. With their $`d_{ij}`$s $`P_j`$ can now calculate $`c_j`$, which itself is again a sharing of a polynomial of degree $`t`$.

## Lecture 8

### Consensus
To achieve consensus, each player has to end up with the same output. The output has to match at least one input of the honest parties.

Consensus can be constructed over multiple rounds and layers. In the following, consensus with information theoretic security is desired. Therefore $`t<\frac{n}{3}`$.

#### Weak Consensus
Here each party sends their input to each other party. If a party receives $`\geq n-t`$ identical inputs, it chooses this input as output. If it has a less clear result, it chooses $`\bot`$. Like this, the output of each party can only either be $`\bot`$ or a single specific value.

#### Graded Consensus
After this round, every party has a value as output. Additionally they also have a grade $`\in\{0,1\}`$. If the grade is 0, the party is not sure whether its choice was accepted by everybody. If it is 1, it is sure that everybody made the same choice. If in the previous round no honest party was insecure then all honest parties have grade 1 in this round.

Parties choose the most occurring input as their output. If an input appeared more or equal than $`n-t`$ times, they can be sure that every body received this input as dominant input and set their grade to 1.

#### King Consensus
A specific party becomes the king. It sends its input to every other party. If the receiving party is still insecure, i.e. has grade set to 0, it will accept the value sent by the king. If not, it will keep its own value. For an honest king, there are 3 scenarios. If the king has grade 1, everyone else has either also grade 1 with the same value, or will take over the kings value. If it has grade 0 and anybody else has grade 1, then everybody already has the same value, and the kings inputs won't change anything. If the king has grade 0 and nobody has grade 1, then everybody will just take the kings value. So if the king is honest, everybody will end up with the same output. If it is malicious, it will preserve all grade 1 values, but otherwise mess things up.

#### Conclusion
To achieve real consensus, the king has to be honest. Therefore the whole process is repeated $`t+1`$ times. Like this, at least once an honest king will be chosen. After that, everybody who is honest agrees on a unique value, which will be consistent through further iterations.

### Broadcast
One player sends its input to everybody else, and all honest players should end up with the same output, that corresponds to the senders input.

## Lecture 9

### Active Adversary

Active adversaries can have multiple levels of being active. Each of them requires new countermeasures.

#### Reading Internal State
If a corrupted party reads its internal state, there is no harm, since there is not more information to extract as for a passively corrupted party.

#### Withholding Messages
If during reconstruction a message is withheld, it can still be reconstructed with the other $`t+1 \geq n-t`$ shares.

If during initial input sending a message is withheld, the party that should have received it can just request the sender to broadcast it. If it refuses, a default value is used.

If during the reconstruction of the shares of a multiplication a share is withheld, it cannot just be reconstructed, because there are not enough honest values, since $`2t+2`$ shares would be needed. Therefore, either the malicious player is excluded and the protocol repeated, or the shares $`a_i, b_i`$ are reconstructed and $`d_i`$ is shared with a default sharing.

#### Sending Wrong Values
To be able to detect wrong values, all player commit to their values they use. If they compute a new value out of it, they do a proof that the computation was correct, and if they transfer a value to an other player, the commitment is transferred as well. If a value has to be broadcasted, the commitment is revealed.

### Multi Party Commitments

#### Sharings
All commitments are broadcasted to everybody. All openings as well.

#### Linear Operations
Since a homomorphism commitment scheme is used, if an addition is made, the commitments of the two values can simply be multiplied.

#### Multiplications
P knows $`a,b`$ and wants to commit to $`ab`$. With peterson commitment $`a`$ and $`b`$ are committed as follows: $`A=g^ah^\alpha,B=g^bh^\beta`$. $`C=g^ch^\gamma`$ is also calculated. $`A,B`$ and $`C`$ are broadcasted. $`B^a`$ can also safely be broadcasted. P now wants to prove, that it knows a $`\xi`$ such that $`C=g^ch^\gamma=g^{ab}h^{\alpha\beta}=B^ag^0h^\xi=g^{ab}h^{a\beta+\xi}`$, where $`\xi=\gamma-a\beta`$. Therefore it has to perform a zero knowledge proof that it knows $`a, \alpha, \xi`$. 

## Lecture 10

### Information Security Commitment Protocol

#### Commit
The dealer generates a random 2-dimensional degree $`(\leq t, \leq t)`$ polynomial $`f(x,y)`$. It describes a surface in the 3-dimensional space. Each player $`P_i`$ receives two functions, $`k_i(x)=f(x,a_i)`$ and $`h_i(y)=f(a_i,y)`$, where $`s_i = k_i(0)`$ is its share.

It then sends to each player $`P_j`$ $`k_i(a_j)`$ and $`h_i(a_j)`$. Like this, if $`P_i`$ received at least $`t+1`$ correct value pairs, it knows, that its polynomials are valid. After $`t`$ other pairs, it can be sure that it received $`\geq t+1`$ value pairs from honest players.

#### Open
To open, the dealer broadcasts $`g(x)`=f(x,0)$. Players check if their share lays on the polygon, and finally the secret was $`s=g(0)`$.

#### Accusations
If something is not correct, players can always accuse the dealer, which then has to broadcast the corresponding values/polynomials. If more than $`t`$ players accuse the dealer, it is disqualified.

#### Transfer
If $`P`$ wants to transfer to $`P'`$, $`P`$ sends $`g(x)`$ to $`P'`$. Everybody sends their $`s_i`$ to $`P'`$. If it receives more than $`2t`$ matching values, it is fine.

#### Multiplication
$`P`$ commits to $`a,b`$ and $`c=ab`$. Then it shares all commitments. It also commits to $`c`$ again, using a polynomial that is the product of the polynomial it used for $`a`$ and $`b`$. The other players check if $`a_i*b_i=c_i`$, and if nobody complains, the first commitment (with the polynomial of degree $`t`$) is accepted.

## Lecture 11

### Hyper Invertible Matrices
They represent a linear function. If the function has $`n`$ inputs, one can reconstruct all input or output values from at least n of those values. Such a function can e constructed via a n-dimensional polynomial. 

### Passive Scenario

#### Sharing
It is shared via shamir sharings.
#### Reconstruction
All values are sent to a single party, which reconstructs the value and sends it to everybody else.

#### Multiplication
$`a,b`$ are shared. $`P_i`$ has $`a_i,b_i`$. They are now $`[a]_t, [b]_t`$. If multiplied, one gets $`[c]_{2t}`$. However, desired is $`[c]_t`$. Then, it is randomized by subtracting $`[r]_{2t}`$, $`[s]_{2t}=[e]_{2t} - [r]_{2t}`$. Now, $`s`$ is publicly reconstructed. It can then simply be added to $`[r]_t`$. Therefore $`[c]_t = [r]_t + s`$.

#### Random Sharings
One first has to efficiently obtain random sharings. Therefore everybody shares a $`s_i`$. A vector of length $`n`$ of those sharings is then locally multiplied with a hyper invertible matrix. The result are $`n-t`$ sharings of random values. Finally, the function from the honest n-t inputs to the n-t outputs is a bijection. It is defined by the dishonest t inputs, but still completely dependent on the n-t honest inputs.

#### Random Double Sharings
Same as above, but $`s_i`$ is shared just twice, once with degree $`t`$ and once with degree $`2t`$.

## Lecture 12

### Active Scenario

#### Player Elimination Framework
If somebody complains, a leader is chosen. Everybody then sends all random values and received messages to the leader. It accuses two parties of cheating. Either the parties accept, then they are eliminated. Otherwise the leader and the party that did not accept are excluded.

#### Sharing
Values are shared via an extended version of Shamir sharings. If a party $`P_i`$ wants to share $`s`$, it takes a random sharing $`r`$ and lets the others reconstruct it towards $`P_i`$. Then $`P_i`$ calculates $`e=s-r`$ and broadcasts $`e`$. The other parties then calculate $`[s]_k = [r]_k + e`$ as their share of the original value. Like this it is guaranteed that $`[s]_k`$ really lie on a polynomial of degree $`t`$.

#### Local Reconstruction
A player has to receive $`d+t+1`$ shares, where d is the degree of the polynomial. If all of these lay on a single polynomial, then it has to be correct, because the polynomial is already completely defined by the $`d+1`$ honest shares.

#### Public Reconstruction
Values $`s_0 \dots s_k`$ are shared. A player now interprets them as coefficients of a polynomial $`g(x) = s_0 + s_1x + \dots + s_kx^k`$ and calculates shares of its value at position $`a_i`$, $`[u_i]_d=[s_0]_d+[s_1]_da_i+ \dots + [s_k]_da_i^{k}`$. This results in $`n`$ shared $`u_i`$ values. These are then locally reconstructed at one player each. These then broadcast the values and everyone can locally find the polynomial that fits through this points.

#### Multiplication
Note that $`x*y = ((x-a)+a)((y-b)+b) = u*v + u*b +v*a - a*b`$, where $`u=x-a, v=y-b`$. Therefore, if $`[a]_k[b]_k=[c]_k`$ are known and random, $`u,y`$ can be calculated and publicly reconstructed, since they are also random. $`[xy]_k = uv + u[b]_k + v[a]_k - [c]_k`$ is then the sharing of degree $`k`$ of the multiplication.

Those triples $`[a]_k,[b]_k,[c]_k`$ can be constructed as follows: First, create random double sharings $`[a]_{t,t'}, [b]_{t,t'}, [r]_{t,2t'}`$. Then calculate and publicly reconstruct $`[s]_{2t'} = [a]_{t'}[b]_{t'} - [r]_{2t'}`$. $`[c]_t = s + [r]_t`$.

#### Random Sharings
Take a n-n hyper invertible matrix. Every player now shares a random value $`s_i`$. Then, each player multiplies the vector with received n sharings with the matrix. Then, 2t of the resulting n sharings of random values $`r_i`$ are reconstructed towards 2t players. If one detects that the value $`r_i`$ is not correct, i.e. has wrong degree, it complains. Since at least t of the checking players are honest, and there are more than n-2t honest inputs to the matrix multiplication, everything must be correct. The reconstructed $`r_i`$s are then discarded because they are already leaked, and the remaining $`n-2t`$ random value sharings can be used.

#### Random Double Sharings
Same as above, but where $`s_i`$ is shared once with degree t and once with degree 2t. The checking players now also check if the reconstructed values of two shares of the same $`r`$ with different degree result in the same value. 